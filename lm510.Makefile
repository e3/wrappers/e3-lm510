where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile



############################################################################
#
# Add any required modules here that come from startup scripts, etc.
#
############################################################################

REQUIRED += stream
ifneq ($(strip $(STREAM_DEP_VERSION)),)
stream_VERSION:=$(STREAM_DEP_VERSION)
endif

############################################################################
#
# If you want to exclude any architectures:
#
############################################################################



############################################################################
#
# If you want to allow specific architectures only:
#
############################################################################

EXCLUDE_ARCHS += linux-corei7-poky
EXCLUDE_ARCHS += linux-ppc64e6500


############################################################################
#
# Relevant directories to point to files
#
############################################################################

APP:=lm510App
APPDB:=$(APP)/Db
APPSRC:=$(APP)/src


############################################################################
#
# Add any files that should be copied to $(module)/Db
#
############################################################################

TEMPLATES += $(wildcard $(APPDB)/*.db)
TEMPLATES += $(patsubst %.substitutions,%.db,$(wildcard $(APPDB)/*.substitutions))
TEMPLATES += $(wildcard $(APPDB)/*.proto)


############################################################################
#
# Add any startup scripts that should be installed in the base directory
#
############################################################################

SCRIPTS += $(wildcard $(APP)/../iocsh/*.iocsh)


############################################################################
#
# If you have any .substitution files, and template files, add them here.
#
############################################################################

SUBS=$(wildcard $(APPDB)/*.substitutions)

USR_DBFLAGS += -I . -I ..
USR_DBFLAGS += -I $(EPICS_BASE)/db
USR_DBFLAGS += -I $(APPDB)

.PHONY: vlibs
vlibs:
